/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package pe.uni.jmenendeza;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestCircle {

    public static void main(String[] args) {
        System.out.println("Test Circle!");
        
        Circle circle1;
        circle1 = new Circle();
        System.out.println(circle1.toString());
        System.out.println("perimeter 1: " + circle1.getPerimeter());
        System.out.println("area 1: " + circle1.getArea()); 
        
        double radious2 = 25;
        Circle circle2;
        circle2 = new Circle(radious2);
        System.out.println(circle2.toString());
        System.out.println("perimeter 2: " + circle2.getPerimeter());
        System.out.println("area 2: " + circle2.getArea());
        
        Circle circle3 = new Circle();
        double radious3 = 125;
        circle3.setRadious(radious3);
        System.out.println(circle3.toString());
        System.out.println("perimeter 3: " + circle3.getPerimeter());
        System.out.println("area 3: " + circle3.getArea());
        

    }
}
