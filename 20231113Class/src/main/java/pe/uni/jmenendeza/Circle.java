/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class Circle {

    double radious;
    
    Circle(){
        this.radious = 1;
    }
    
    Circle(double newRadius){
        this.radious = newRadius;
    }
    
    double getArea(){
        return Math.PI*this.radious*this.radious;
    }
    
    double getPerimeter(){
        return 2*Math.PI*this.radious;
    }
    
    void setRadious(double newRadious){
        this.radious = newRadious;
    }

    @Override
    public String toString() {
        return "Circle{" + "radious=" + radious + '}';
    }
    
}
