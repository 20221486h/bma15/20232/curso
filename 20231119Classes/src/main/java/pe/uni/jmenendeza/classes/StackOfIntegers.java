/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.classes;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class StackOfIntegers {
    private int[] elements;
    private int size;
    public static final int DEFAULT_CAPACITY = 16;
    
    public StackOfIntegers(){
//        elements = new int[DEFAULT_CAPACITY];
        this(DEFAULT_CAPACITY);
    }
    
    public StackOfIntegers(int capacity){
        elements = new int[capacity];
    }
    
    public boolean empty(){
        return(size == 0);
    }
    
    public int peek(){
        return(elements[size-1]);
    }

    public int getSize() {
        return size;
    }
    
    /**
     * solo agrega enteros hasta la capacidad definida
     * @param value 
     */
    public void push(int value){
        if(elements.length > size){
            elements[size++] = value;
        }
    }
    
    public int pop(){
        return elements[--size]; //solo lectura, falta implementar
    }
}
