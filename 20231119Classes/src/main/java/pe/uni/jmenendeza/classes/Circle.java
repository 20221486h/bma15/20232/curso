/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.classes;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class Circle extends GeometricObject {
    private double radious;
    
    public Circle(){   
    }
    
    public Circle(double radious){
       this.radious = radious; 
    }
    
    public Circle(double radius, String color, boolean filled){
        this.radious = radious;
        setColor(color);
        setFilled(filled);
    }

    public double getRadious() {
        return radious;
    }

    public void setRadious(double radious) {
        this.radious = radious;
    }
    
    double getDiameter(){
        return 2 * radious;
    }
    
    double getPerimeter(){
        return 2 * Math.PI * radious;
    }
    
    double getArea(){
        return Math.PI * radious * radious;
    }
    
    void printCircle(){
        System.out.println("Creacion: " + getDateCreated() + " radio: " + this.radious);
    }
   
}
