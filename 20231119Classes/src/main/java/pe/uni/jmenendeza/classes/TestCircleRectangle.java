/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.classes;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestCircleRectangle {
    public static void main(String[] args) {
        Circle circle = new Circle(1);
        System.out.println("Un circulo: " + circle.toString( ));
        System.out.println("El color: " + circle.getColor());
        System.out.println("El radio: " + circle.getRadious());
        System.out.println("El area: " + circle.getArea());
        System.out.println("El diametro: " + circle.getDiameter());
    }
}
