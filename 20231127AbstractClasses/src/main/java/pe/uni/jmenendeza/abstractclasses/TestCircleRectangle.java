/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.abstractclasses;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestCircleRectangle {
    
    public static void main(String[] args) {
        System.out.println("TestCircleRectangle!!!");
        
        Circle circle = new Circle(5);
        Rectangle rectangle = new Rectangle(5, 5);
        System.out.println("El circulo: " + circle.toString());
        System.out.println("El area del circulo: " + circle.getArea());
        System.out.println("El perimetro del circulo: " + circle.getPerimeter());
        System.out.println("El rectangulo: " + rectangle.toString());
        System.out.println("El area del rectangulo: " + rectangle.getArea());
        System.out.println("El perimetro del rectangulo: " + rectangle.getPerimeter());
    }
}
