/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.jmenendeza.exceptions;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Exceptions !!!");
        int[] array = {10, 20, 30, 40, 50, 60};
        int i=0;
        /*
        if(i < array.length){
            System.out.println(array[i]);
        }else {
            System.out.println("Fuera de los limites");
        }
        */
        try{
            //abrir la base de datos
            System.out.println(array[i]);
//            int j = 10/i;
            Function(i);
            // cerrar la base de datos
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Fuera de los limites");
            System.out.println(e);
        }catch (ArithmeticException e){
            System.out.println("Division entre cero");
            System.out.println(e);
        }catch (Exception e){
            System.out.println("Default !!!");
            System.out.println(e);
        }finally{
            System.out.println("Cierra la base de datos");
        }
    }
    
    public static void Function(int i) throws ArithmeticException{
        int j = 10/i;
    }
}
