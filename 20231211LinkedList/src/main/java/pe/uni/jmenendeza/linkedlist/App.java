package pe.uni.jmenendeza.linkedlist;

import java.util.Collections;
import java.util.LinkedList;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {

    private final LinkedList<Integer> list = new LinkedList<>();
    private final Label lbMessage = new Label("Ingrese un numero: ");
    private final TextField tfNumber = new TextField();
    private final TextArea taNumbers = new TextArea();
    private final Button btSort = new Button("Ordenar");
    private final Button btShuffle = new Button("Aleatorio");
    private final Button btReverse = new Button("Invertir");
    
    @Override
    public void start(Stage stage) {
//        var javaVersion = SystemInfo.javaVersion();
//        var javafxVersion = SystemInfo.javafxVersion();
//
//        var label = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
//        var scene = new Scene(new StackPane(label), 640, 480);

        HBox hBox = new HBox(10);
        hBox.getChildren().addAll(lbMessage, tfNumber);
        hBox.setAlignment(Pos.CENTER);
        
        HBox hBoxForButtons = new HBox();
        hBoxForButtons.getChildren().addAll(btSort, btShuffle, btReverse);
        hBoxForButtons.setAlignment(Pos.CENTER);

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(hBox);
        borderPane.setCenter(new ScrollPane(taNumbers));
        borderPane.setBottom(hBoxForButtons);
        
        var scene = new Scene(borderPane, 400, 220);
        stage.setScene(scene);
        stage.show();
        
        tfNumber.setOnAction(e -> {
            System.out.println("Ingresar numero!");
            String sNumber = tfNumber.getText();
            if (!list.contains(Integer.valueOf(sNumber))){
                list.add(Integer.valueOf(sNumber));
                taNumbers.appendText( sNumber + " ");
            }
            tfNumber.setText("");
        });
        
        btSort.setOnAction(e -> {
            System.out.println("ordenar!");
            Collections.sort(list);
            display();
        });
        
        btShuffle.setOnAction(e -> {
            System.out.println("aleatorio!");
            Collections.shuffle(list);
            display();
        });
        
        btReverse.setOnAction(e -> {
            System.out.println("reversa!");
            Collections.reverse(list);
            display();
        });
    }
    
    private void display(){
        taNumbers.setText(null);
        for (Integer integer : list) {
            taNumbers.appendText(integer + " ");
        }
    }

    public static void main(String[] args) {
        launch();
    }

}