/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.classexample;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TV {
    int channel;
    int volumeLevel;
    boolean on;

    public boolean isOn() {
        return on;
    }
    
    //limites
    final int minChannel = 1;
    final int maxChannel = 120;
    final int minVolumeLevel = 1;
    final int maxVolumeLevel = 7;

    public TV() {
        this.channel = 1;
        this.volumeLevel = 3;
        this.on = false;
    }
    
    public void turnOn(){
        on = true;
    }
    
    public void turnOff(){
        on = false;
    }
    
    public void setChannel(int newChannel){
        this.channel = newChannel;
    }
    
    public void setVolume(int newVolumeLevel){
        this.volumeLevel = newVolumeLevel;
    }
    
    public void channelUp(){
        if(on && this.channel < maxChannel){
            this.channel++;
        }else if(this.channel == maxChannel){
            this.channel = minChannel;
        }
    }

    public void channelDown(){
        if(on && this.channel > minChannel){
            this.channel--;
        }else if(this.channel == minChannel){
            this.channel = maxChannel;
        }
    }
    
    public void VolumeUp(){
        if(on && this.volumeLevel < maxVolumeLevel){
            this.volumeLevel++;
        }
    }

    public void VolumeDown(){
        if(on && this.volumeLevel > minVolumeLevel){
            this.volumeLevel--;
        }
    }

    @Override
    public String toString() {
        return "TV{" + "channel=" + channel + ", volumeLevel=" + volumeLevel + ", on=" + on + '}';
    }
    
}