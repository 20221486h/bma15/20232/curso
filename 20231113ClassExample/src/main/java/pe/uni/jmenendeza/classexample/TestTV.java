/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package pe.uni.jmenendeza.classexample;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestTV {

    public static void main(String[] args) {
       TV tv1 = new TV();
       tv1.turnOn();
       //System.out.println(tv1.toString());
       if(tv1.isOn()){
           System.out.println("Encendido");
           int channel = 120;
           int volumeLevel = 7;
           tv1.setChannel(channel);
           tv1.setVolume(volumeLevel);
           System.out.println(tv1.toString());
           tv1.channelUp();
           System.out.println(tv1.toString());
       }else {
           System.out.println("Apagado");
       }
       
    }
}
