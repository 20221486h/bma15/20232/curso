/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.collections;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class GeometricObjectComparator implements Comparator<GeometricObject>, Serializable {

    @Override
    public int compare(GeometricObject o1, GeometricObject o2) {
        double area1 = o1.getArea();
        double area2 = o2.getArea();
        if (area1 == area2){
            return 0;
        }else if (area1 > area2){
            return 1;
        }else {
            return -1;
        }
    }
    
}
