/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.collections;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestSortStringIgnoreCase {
    public static void main(String[] args) {
        System.out.println("TestSortStringIgnoreCase!");
        List<String> cities = Arrays.asList("Arequipa", "Abancay", "Lima", "La Libertad", "Piura", "Oxapampa", "Callao", "Junin", "Iquitos");
        for (String city : cities) {
            System.out.print(city + " ");
        }
        System.out.println("");
        // funcion lambda: e -> f(e); (x, y) -> f(x, y)
        cities.sort((s1, s2) -> s1.compareToIgnoreCase(s2));
        for (String city : cities) {
            System.out.print(city + " ");
        }
    }
}
