/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestIterator {
    public static void main(String[] args) {
        System.out.println("TestIterators!");
        String[] cities = {"Arequipa", "Lima", "Piura", "Oxapampa", "Callao", "Junin", "Iquitos"};
        
        Collection<String> collection = new ArrayList<>();
        collection.add(cities[0]);
        collection.add(cities[1]);
        collection.add(cities[2]);
        collection.add(cities[3]);
        
        Iterator<String> iterator = collection.iterator();
        
        while (iterator.hasNext()) {
            String next = iterator.next();
            System.out.print(next.toUpperCase() + " ");
        }
        System.out.println("");
        
        for (String string : collection) {
            System.out.print(string.toLowerCase() + " ");
        }
        System.out.println("");
        
        collection.forEach(e -> System.out.print(e.toUpperCase() + " "));
    }
}
