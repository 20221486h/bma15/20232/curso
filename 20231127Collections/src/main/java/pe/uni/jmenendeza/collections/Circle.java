/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.collections;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class Circle extends GeometricObject {
    
    private double radious;

    public double getRadious() {
        return radious;
    }

    public void setRadious(double radious) {
        this.radious = radious;
    }

    public Circle() {
        radious = 1.0;
    }

    public Circle(double radious) {
        this.radious = radious;
    }

    public Circle(double radious, String color, boolean filled) {
        super(color, filled);
        this.radious = radious;
    }
    
    public double getDiameter(){
        return 2 * radious;
    }
    
    @Override
    public double getArea() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        return Math.PI * radious * radious;
    }

    @Override
    public double getPerimeter() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        return 2 * Math.PI * radious;
    }

    @Override
    public String toString() {
        return "Circle{" + "radious=" + radious + '}';
    }
    
}
