/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.collections;

import java.util.Comparator;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestComparator {
    public static void main(String[] args) {
        System.out.println("TestComparator!");
        GeometricObject  go1 = new Rectangle(5, 5);
        GeometricObject  go2 = new Circle(5);
        
//        System.out.println("Ccomparacion: " + comparar(go1, go2));
        
        GeometricObject g0 = mayor(go1, go2, new GeometricObjectComparator());
        System.out.println("El area de l objeto mas grade: " + g0.getArea());
    }
    
    private static GeometricObject mayor(GeometricObject g1, GeometricObject g2, Comparator<GeometricObject> c){
        if (c.compare(g1, g2) >= 0){
            return g1;
        }else {
            return g2;
        }
    }
    
    /*
    private static int comparar(GeometricObject o1, GeometricObject o2){
        double area1 = o1.getArea();
        double area2 = o2.getArea();
        if (area1 == area2){
            return 0;
        }else if (area1 > area2){
            return 1;
        }else {
            return -1;
        }
    }
    */
}
