/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestArrayAndLinkedList {
    public static void main(String[] args) {
        System.out.println("TestArrayAndLinkedList!");
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(1);
        arrayList.add(4);
        System.out.println("Una lista de enteros en la lista array: " + arrayList);
        arrayList.add(0, 10);
        System.out.println("Una lista de enteros en la lista array: " + arrayList);
        arrayList.add(3, 30);
        System.out.println("Una lista de enteros en la lista array: " + arrayList);
        
        LinkedList<Object> linkedList = new LinkedList<>(arrayList);
        System.out.println("Muestra la lista enlazada: " + linkedList);
        linkedList.forEach(e -> System.out.print(e + " "));
        System.out.println("");
        linkedList.add(1, "red");
        linkedList.forEach(e -> System.out.print(e + " "));
        System.out.println("");
        linkedList.removeLast();
        linkedList.forEach(e -> System.out.print(e + " "));
        System.out.println("");
        linkedList.addFirst("green");
        linkedList.forEach(e -> System.out.print(e + " "));
        System.out.println("");
        
        System.out.println("Muestra la lista enlazada hacia adelante:");
        ListIterator<Object> listIterator;
        
        listIterator = linkedList.listIterator();
        while (listIterator.hasNext()) {
            Object next = listIterator.next();
            System.out.print(next + " ");
        }
        System.out.println("");
        
        System.out.println("Muestra la lista enlazada hacia atras:");
        listIterator = linkedList.listIterator(linkedList.size());
        while (listIterator.hasPrevious()) {
            Object previous = listIterator.previous();
            System.out.print(previous + " ");
        }
        System.out.println("");
        
    }
}
