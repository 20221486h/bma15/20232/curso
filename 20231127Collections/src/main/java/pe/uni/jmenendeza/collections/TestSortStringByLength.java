/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.collections;

import java.util.Arrays;
import java.util.Comparator;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestSortStringByLength {
    public static void main(String[] args) {
        System.out.println("TestSortStringByLength!");
        String[] cities = {"Arequipa", "Lima", "Piura", "Oxapampa", "Callao", "Junin", "Iquitos"};
        for (String city : cities) {
            System.out.println(city + ", " + city.length());
        }
        System.out.println("ciudades: " + Arrays.toString(cities));
        Arrays.sort(cities, new MyComparator());
        System.out.println("ciudades: " + Arrays.toString(cities));
    }
    
    public static class MyComparator implements Comparator<String>{

        @Override
        public int compare(String o1, String o2) {
            return o1.length() - o2.length();
        }
    }
}
