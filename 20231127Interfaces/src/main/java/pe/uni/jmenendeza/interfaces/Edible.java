/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pe.uni.jmenendeza.interfaces;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public interface Edible {
    public String howToEat();
}
