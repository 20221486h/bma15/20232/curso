/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.interfaces;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class Orange extends Fruit {

    @Override
    public String howToEat() {
        return "Orange: make an orange juice!";
    }
    
}
