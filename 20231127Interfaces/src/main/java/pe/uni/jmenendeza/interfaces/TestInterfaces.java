/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.uni.jmenendeza.interfaces;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class TestInterfaces {

    public static void main(String[] args) {
        System.out.println("TestInterfaces!");
        Object[] objects = {new Orange(), new Apple(), new Chicken(), new Tiger()};
        
        for (Object object : objects){
            if (object instanceof Animal) {
                System.out.println(((Animal) object).sound());
            }
            if(object instanceof Edible){
                System.out.println(((Edible) object).howToEat());
            }
            
        }
        
    }
}
