/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.jmenendeza.interfaces;

/**
 *
 * @author Jose Menendez <jose.menendez.a@uni.pe>
 */
public class Chicken extends Animal implements Edible {

    @Override
    public String sound() {
        return "Chicken: ki ki ri ki!";
    }

    @Override
    public String howToEat() {
        return "Chiken: fried it!!!";
    }
    
}
